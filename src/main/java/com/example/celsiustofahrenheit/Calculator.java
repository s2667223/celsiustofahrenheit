package com.example.celsiustofahrenheit;

public class Calculator {

    public double convert(String celsius) {
        return (Double.parseDouble(celsius) * 1.8) + 32;
    }
}
